# openjdk*-jre

Java Runtime https://pkgs.alpinelinux.org/packages?name=openjdk*-jre&branch=edge&arch=x86_64

## Versionless package name: java-jre

## Version number seems to be needed in package name!
This may be a problem when numerous docker tests have to be maintained.

May I suggest other Linux distributions for this type of use:
* Arch Linux
* Ubuntu - Debian
* Fedora - Centos
* OpenSUSE

For some examples, have a look at:
* [hub-docker-com-demo/clojure](https://gitlab.com/hub-docker-com-demo/clojure)